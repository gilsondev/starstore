# Star Wars Store
[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=starstore&metric=alert_status)](https://sonarcloud.io/dashboard?id=starstore)
[![Coverage](https://sonarcloud.io/api/project_badges/measure?project=starstore&metric=coverage)](https://sonarcloud.io/dashboard?id=starstore)
[![Code Smells](https://sonarcloud.io/api/project_badges/measure?project=starstore&metric=code_smells)](https://sonarcloud.io/dashboard?id=starstore)
[![Technical Debt](https://sonarcloud.io/api/project_badges/measure?project=starstore&metric=sqale_index)](https://sonarcloud.io/dashboard?id=starstore)

Projeto para exemplo de uma aplicação desenvolvida com as seguintes tecnologias:

- Spring Boot
- PostgreSQL

## Instalação

- Faça o checkout do projeto:
```shell
$ git clone https://gitlab.com/gilsondev/starstore.git
```

- Entre no diretório e inicie o servidor local:
```shell
$ ./mvnw spring-boot:run
```

## Testes automatizados

Para rodar todos os testes use o `./mvnw test`.