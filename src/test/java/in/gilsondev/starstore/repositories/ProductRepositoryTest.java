package in.gilsondev.starstore.repositories;

import in.gilsondev.starstore.builders.ProductBuilder;
import in.gilsondev.starstore.entities.Product;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.persistence.EntityManager;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
public class ProductRepositoryTest {
    @Autowired
    private EntityManager entityManager;

    @Autowired
    private ProductRepository productRepository;

    @Test
    public void shouldReturnAllProducts() {
        Product product = ProductBuilder.newProduct().build();
        entityManager.persist(product);
        entityManager.flush();

        List<Product> products = productRepository.findAll();
        assertThat(products, hasSize(1));
    }
}
