package in.gilsondev.starstore.resources;

import com.fasterxml.jackson.databind.ObjectMapper;
import in.gilsondev.starstore.builders.ProductBuilder;
import in.gilsondev.starstore.entities.Product;
import in.gilsondev.starstore.repositories.ProductRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class ProductResourceTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ProductRepository productRepository;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    public void sholdReturnStatusOK() throws Exception {
        mockMvc.perform(
                get("/starstore/products").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void shouldReturnProductList() throws Exception {
        Product product = ProductBuilder.newProduct().build();

        List<Product> products = Arrays.asList(product);
        when(productRepository.findAll()).thenReturn(products);

        mockMvc.perform(
                get("/starstore/products").contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].title", is("Blusa do Imperio")))
                .andExpect(jsonPath("$[0].price", is(7990)))
                .andExpect(jsonPath("$[0].zipcode", is("78993-000")))
                .andExpect(jsonPath("$[0].seller", is("João da Silva")))
                .andExpect(jsonPath("$[0].thumbnailHD", is("https://cdn.awsli.com.br/600x450/21/21351/produto/3853007/f66e8c63ab.jpg")))
                .andExpect(jsonPath("$[0].date", is("26/11/2015")));
    }

    @Test
    public void shouldCreateANewProduct() throws Exception {
        Product product = ProductBuilder.newProduct().build();
        Product productCreated = ProductBuilder.newProduct().withID(1L).build();

        String data = objectMapper.writeValueAsString(product);

        when(productRepository.save(product)).thenReturn(productCreated);

        MockHttpServletResponse response = mockMvc.perform(post("/starstore/products")
                .accept(MediaType.APPLICATION_JSON)
                .content(data).contentType(MediaType.APPLICATION_JSON)).andReturn().getResponse();

        assertEquals(HttpStatus.CREATED.value(), response.getStatus());
        assertEquals("http://localhost/starstore/products/1", response.getHeader(HttpHeaders.LOCATION));
    }
}
