package in.gilsondev.starstore.builders;

import in.gilsondev.starstore.entities.Product;

public class ProductBuilder {
    private static Product product;

    private ProductBuilder() {}

    public static ProductBuilder newProduct() {
        product = new Product();
        product.setTitle("Blusa do Imperio");
        product.setPrice(7990);
        product.setZipcode("78993-000");
        product.setSeller("João da Silva");
        product.setThumbnailHD("https://cdn.awsli.com.br/600x450/21/21351/produto/3853007/f66e8c63ab.jpg");
        product.setDate("26/11/2015");

        return new ProductBuilder();
    }

    public ProductBuilder withID(Long id) {
        product.setId(id);
        return this;
    }

    public ProductBuilder withTitle(String title) {
        product.setTitle(title);
        return this;
    }

    public Product build() {
        return product;
    }
}
