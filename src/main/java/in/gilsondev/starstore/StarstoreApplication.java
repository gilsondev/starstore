package in.gilsondev.starstore;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StarstoreApplication {

	public static void main(String[] args) {
		SpringApplication.run(StarstoreApplication.class, args);
	}

}
