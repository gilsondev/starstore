package in.gilsondev.starstore.resources;

import in.gilsondev.starstore.entities.Product;
import in.gilsondev.starstore.repositories.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;

@RestController
@RequestMapping("/starstore/products")
public class ProductResource {
    @Autowired
    private ProductRepository productRepository;

    @GetMapping
    public List<Product> listProducts() {
        return productRepository.findAll();
    }

    @PostMapping
    public ResponseEntity<Void> registerProduct(@RequestBody Product productData) {
        Product product = productRepository.save(productData);

        URI location = ServletUriComponentsBuilder.fromCurrentRequest().path(
                "/{id}").buildAndExpand(product.getId()).toUri();

        return ResponseEntity.created(location).build();
    }
}
